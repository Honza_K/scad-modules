module rounded_cube(dimensions, radius=0, fn=90)
{
     if (radius == 0)
     {
          cube(dimensions);
     }
     else
     {
          eps=0.1;
          rad = min(min(dimensions)/2, radius);
          inner_dimensions=dimensions-[2*rad,2*rad,2*rad];

          translate([rad,rad,rad])
               cube(inner_dimensions);

          translate([rad,rad,0])
               cube([inner_dimensions.x, inner_dimensions.y, rad+eps]);
          translate([rad,rad,rad+inner_dimensions.z-eps])
               cube([inner_dimensions.x, inner_dimensions.y, rad+eps]);

          translate([rad,0,rad])
               cube([inner_dimensions.x, rad+eps, inner_dimensions.z]);
          translate([rad,rad+inner_dimensions.y-eps,rad])
               cube([inner_dimensions.x, rad+eps, inner_dimensions.z]);

          translate([0,rad,rad])
               cube([rad+eps, inner_dimensions.y, inner_dimensions.z]);
          translate([rad+inner_dimensions.x-eps,rad,rad])
               cube([rad+eps, inner_dimensions.y, inner_dimensions.z]);

          translate([rad,rad,rad])
               sphere(rad, $fn=fn);
          translate([rad+inner_dimensions.x,rad,rad])
               sphere(rad, $fn=fn);
          translate([rad+inner_dimensions.x,rad+inner_dimensions.y,rad])
               sphere(rad, $fn=fn);
          translate([rad,rad+inner_dimensions.y,rad])
               sphere(rad, $fn=fn);

          translate([rad,rad,rad+inner_dimensions.z])
               sphere(rad, $fn=fn);
          translate([rad+inner_dimensions.x,rad,rad+inner_dimensions.z])
               sphere(rad, $fn=fn);
          translate([rad+inner_dimensions.x,rad+inner_dimensions.y,rad+inner_dimensions.z])
               sphere(rad, $fn=fn);
          translate([rad,rad+inner_dimensions.y,rad+inner_dimensions.z])
               sphere(rad, $fn=fn);

          translate([rad,rad,rad])
               cylinder(h=inner_dimensions.z,r=rad, $fn=fn);
          translate([rad+inner_dimensions.x,rad,rad])
               cylinder(h=inner_dimensions.z,r=rad, $fn=fn);
          translate([rad+inner_dimensions.x,rad+inner_dimensions.y,rad])
               cylinder(h=inner_dimensions.z,r=rad, $fn=fn);
          translate([rad,rad+inner_dimensions.y,rad])
               cylinder(h=inner_dimensions.z,r=rad, $fn=fn);

          translate([rad,rad,rad])
               rotate([0,90,0])
               cylinder(h=inner_dimensions.x,r=rad, $fn=fn);
          translate([rad,rad+inner_dimensions.y,rad])
               rotate([0,90,0])
               cylinder(h=inner_dimensions.x,r=rad, $fn=fn);
          translate([rad,rad,rad+inner_dimensions.z])
               rotate([0,90,0])
               cylinder(h=inner_dimensions.x,r=rad, $fn=fn);
          translate([rad,rad+inner_dimensions.y,rad+inner_dimensions.z])
               rotate([0,90,0])
               cylinder(h=inner_dimensions.x,r=rad, $fn=fn);

          translate([rad,rad+inner_dimensions.y,rad])
               rotate([90,0,0])
               cylinder(h=inner_dimensions.y,r=rad, $fn=fn);
          translate([rad+inner_dimensions.x,rad+inner_dimensions.y,rad])
               rotate([90,0,0])
               cylinder(h=inner_dimensions.y,r=rad, $fn=fn);
          translate([rad,rad+inner_dimensions.y,rad+inner_dimensions.z])
               rotate([90,0,0])
               cylinder(h=inner_dimensions.y,r=rad, $fn=fn);
          translate([rad+inner_dimensions.x,rad+inner_dimensions.y,rad+inner_dimensions.z])
               rotate([90,0,0])
               cylinder(h=inner_dimensions.y,r=rad, $fn=fn);
     }
}
